# CHANGELOG.md

## 1.0.0 (2022-08-27)

Main Release:

- Adding all feature with cursor
- Add Magnetic support for cursor
- Adding Configuration, Project setup, and Documentations
