import gsap, { Expo } from 'gsap'
import React, { FunctionComponent, useEffect, useRef } from 'react'
import './styles/main.scss'

interface AwesomeCursorProps {
  className?: string
  backgroundColor?: string
  initialSize?: number
  animationDuration?: number
  magneticAmount?: number
  cursorAnimationDuration?: number
  enableSpecialElementScale?: boolean
}

/**
 * # AwesomeCursor
 *
 * the custom make cursor to handle some of the event in some
 * element in tags html by using [data-cursor] attribute
 *
 * @returns JSX.Element
 */
const AwesomeCursor: FunctionComponent<AwesomeCursorProps> = ({
  backgroundColor = '#000',
  initialSize = 12.0,
  animationDuration = 1.5,
  magneticAmount = 0.9,
  className,
  cursorAnimationDuration = 0.7,
  enableSpecialElementScale = true,
}): JSX.Element => {
  const cursorRef = useRef<HTMLDivElement | null>(null)
  let cursorOptions: gsap.TweenVars = {
    duration: cursorAnimationDuration,
    x: 0.0,
    y: 0.0,
    scale: 1,
    opacity: 1,
    width: initialSize,
    height: initialSize,
    background: backgroundColor,
    mixBlendMode: 'inherit',
    overwrite: true,
    ease: Expo.easeOut,
    force3D: true,
  }

  const trackCursor: () => void = () => {
    // define the current options with the new one
    // when something happen in options
    cursorOptions = {
      ...cursorOptions,
      width: initialSize,
      height: initialSize,
      background: backgroundColor,
    }

    // specify the cursor pointer event
    // NOTE: this only effect to cursor to move by the
    // current cursor position
    document.body.addEventListener('mouseenter', () => {
      cursorOptions = { ...cursorOptions, opacity: 1 }

      // Start to animate the cursor
      // by all of hooked cursor options
      gsap.to('#awesome-cursor', cursorOptions)
    })

    document.body.addEventListener('mousemove', (e) => {
      const areaTarget = e.target as HTMLElement
      if (enableSpecialElementScale) {
        if (
          areaTarget instanceof HTMLButtonElement ||
          areaTarget instanceof HTMLAnchorElement ||
          areaTarget instanceof HTMLInputElement ||
          areaTarget instanceof HTMLTextAreaElement ||
          areaTarget instanceof HTMLImageElement
        ) {
          cursorOptions = { ...cursorOptions, scale: 0.8 }
        }
      } else {
        cursorOptions = { ...cursorOptions, scale: 1.0 }
      }

      cursorOptions = { ...cursorOptions, x: e.clientX, y: e.clientY }
      gsap.to('#awesome-cursor', cursorOptions)
    })

    document.body.addEventListener('mouseleave', () => {
      cursorOptions = { ...cursorOptions, opacity: 0 }
      gsap.to('#awesome-cursor', cursorOptions)
    })
  }

  const trackElementsCursor: () => void = () => {
    // define the current options with the new one
    // when something happen in options
    cursorOptions = {
      ...cursorOptions,
      width: initialSize,
      height: initialSize,
      background: backgroundColor,
    }

    // define all of the specify cursor element
    // find by using [data-cursor] attribute
    // like text, size, color, and more
    let sizeCursorElements =
      (document.querySelectorAll(
        '[data-cursor-size]'
      ) as unknown as NodeListOf<HTMLElement>) ?? []
    let textCursorElements =
      (document.querySelectorAll(
        '[data-cursor-text]'
      ) as unknown as NodeListOf<HTMLElement>) ?? []
    let colorCursorElements =
      (document.querySelectorAll(
        '[data-cursor-color]'
      ) as unknown as NodeListOf<HTMLElement>) ?? []
    let magneticCursorElements =
      (document.querySelectorAll(
        '[data-cursor-magnetic]'
      ) as unknown as NodeListOf<HTMLElement>) ?? []
    let exclusionCursorElements =
      (document.querySelectorAll(
        '[data-cursor-exclusion]'
      ) as unknown as NodeListOf<HTMLElement>) ?? []
    let imageCursorElements =
      (document.querySelectorAll(
        '[data-cursor-image]'
      ) as unknown as NodeListOf<HTMLElement>) ?? []

    // now specify all of listener in all of the child
    // for all element that we grab with `mouseenter` and `mouseleave`
    sizeCursorElements.forEach((el) => {
      el.addEventListener('mouseenter', (e) => {
        const areaTarget = e.target as HTMLElement
        const size = parseInt(areaTarget.dataset['cursorSize']!)

        cursorOptions = {
          ...cursorOptions,
          width: size,
          height: size,
          top: -size / 2,
          left: -size / 2,
          duration: animationDuration,
        }
      })

      el.addEventListener('mousemove', (e) => {
        const areaTarget = e.target as HTMLElement
        const size = parseInt(areaTarget.dataset['cursorSize']!)

        cursorOptions = {
          ...cursorOptions,
          width: size,
          height: size,
          top: -size / 2,
          left: -size / 2,
          duration: animationDuration,
        }
      })

      el.addEventListener('mouseleave', () => {
        cursorOptions = {
          ...cursorOptions,
          width: initialSize,
          height: initialSize,
          left: 0,
          top: 0,
          duration: cursorAnimationDuration,
        }
      })
    })

    textCursorElements.forEach((el) => {
      el.addEventListener('mouseenter', (e) => {
        const areaTarget = e.target as HTMLElement
        const text = areaTarget.dataset['cursorText']
        const textColor = areaTarget.dataset['cursorTextColor']
        if (cursorRef.current && text) {
          cursorRef.current.textContent = text
        }
        cursorOptions = {
          ...cursorOptions,
          color: textColor,
          duration: animationDuration,
        }
      })

      el.addEventListener('mousemove', (e) => {
        const areaTarget = e.target as HTMLElement
        const text = areaTarget.dataset['cursorText']
        const textColor = areaTarget.dataset['cursorTextColor']
        if (cursorRef.current && text) {
          cursorRef.current.textContent = text
        }
        cursorOptions = {
          ...cursorOptions,
          color: textColor,
          duration: animationDuration,
        }
      })

      el.addEventListener('mouseleave', () => {
        cursorOptions = {
          ...cursorOptions,
          duration: cursorAnimationDuration,
        }
        if (cursorRef.current) {
          cursorRef.current.textContent = ''
        }
      })
    })

    colorCursorElements.forEach((el) => {
      el.addEventListener('mouseenter', (e) => {
        const areaTarget = e.target as HTMLElement
        const color = areaTarget.dataset['cursorColor']

        cursorOptions = {
          ...cursorOptions,
          background: color,
          duration: animationDuration,
        }
      })

      el.addEventListener('mousemove', (e) => {
        const areaTarget = e.target as HTMLElement
        const color = areaTarget.dataset['cursorColor']

        cursorOptions = {
          ...cursorOptions,
          background: color,
          duration: animationDuration,
        }
      })

      el.addEventListener('mouseleave', () => {
        cursorOptions = {
          ...cursorOptions,
          background: backgroundColor,
          duration: cursorAnimationDuration,
        }
      })
    })

    exclusionCursorElements.forEach((el) => {
      el.addEventListener('mouseenter', (e) => {
        const areaTarget = e.target as HTMLElement
        const exclusionColor =
          areaTarget.dataset['cursorExclusionColor'] ?? '#fff'

        cursorOptions = {
          ...cursorOptions,
          mixBlendMode: 'exclusion',
          background: exclusionColor,
          duration: animationDuration,
        }
      })

      el.addEventListener('mousemove', (e) => {
        const areaTarget = e.target as HTMLElement
        const exclusionColor =
          areaTarget.dataset['cursorExclusionColor'] ?? '#fff'

        cursorOptions = {
          ...cursorOptions,
          mixBlendMode: 'exclusion',
          background: exclusionColor,
          duration: animationDuration,
        }
      })

      el.addEventListener('mouseleave', () => {
        cursorOptions = {
          ...cursorOptions,
          mixBlendMode: 'inherit',
          background: backgroundColor,
          duration: cursorAnimationDuration,
        }
      })
    })

    imageCursorElements.forEach((el) => {
      el.addEventListener('mouseenter', (e) => {
        const areaTarget = e.target as HTMLElement
        const image = areaTarget.dataset['cursorImage']

        cursorOptions = {
          ...cursorOptions,
          background: `url("${image}")`,
          duration: animationDuration,
        }
      })

      el.addEventListener('mousemove', (e) => {
        const areaTarget = e.target as HTMLElement
        const image = areaTarget.dataset['cursorImage']

        cursorOptions = {
          ...cursorOptions,
          background: `url("${image}")`,
          duration: animationDuration,
        }
      })

      el.addEventListener('mouseleave', () => {
        cursorOptions = {
          ...cursorOptions,
          background: backgroundColor,
          duration: cursorAnimationDuration,
        }
      })
    })

    magneticCursorElements.forEach((el) => {
      el.addEventListener('mousemove', (e) => {
        const areaTarget = e.currentTarget as HTMLElement

        // start the magnetic effect
        gsap.to(areaTarget, {
          x:
            (e.clientX -
              (areaTarget.offsetLeft - window.pageXOffset) -
              areaTarget.clientWidth / 2) *
            magneticAmount!,
          y:
            (e.clientY -
              (areaTarget.offsetTop - window.pageYOffset) -
              areaTarget.clientHeight / 2) *
            magneticAmount!,
          duration: animationDuration,
          ease: Expo.easeOut,
          force3D: true,
        })
      })

      el.addEventListener('mouseleave', (e) => {
        const areaTarget = e.currentTarget as HTMLElement

        // stop the magnetic effect
        gsap.to(areaTarget, {
          x: 0,
          y: 0,
          duration: animationDuration,
          ease: Expo.easeOut,
        })
      })
    })
  }

  useEffect(() => {
    trackCursor()
  })

  useEffect(() => {
    trackElementsCursor()
  })

  return (
    <div
      id="awesome-cursor"
      className={`awesome-cursor ${className}`}
      ref={cursorRef}
    ></div>
  )
}

export default AwesomeCursor
