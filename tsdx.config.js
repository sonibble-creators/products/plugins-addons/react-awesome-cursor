/**
 * TSDX Configuration File
 *
 * Configuratin file for TSDX
 * Zero configuration typescript package library cli
 */

const scss = require("rollup-plugin-scss")

module.exports = {
  rollup(config, options) {
    config.plugins.push(
      scss({
        output: "dist/main.css",
        inject: false,
        extract: !!options.writeMeta,
      })
    )
    return config
  },
}
